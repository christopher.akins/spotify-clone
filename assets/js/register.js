

$(function() {
  $('#hideLogin').on('click', function() {
    $('.register-wrapper').css('display', 'block');
    $('.login-wrapper').css('display', 'none');
  });

  $('#hideRegister').on('click', function() {
    $('.register-wrapper').css('display', 'none');
    $('.login-wrapper').css('display', 'block');
  });
});