-- CREATE the Songs table
DROP TABLE IF EXISTS `songs`;
CREATE TABLE songs(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, title varchar(250), artist INT, album INT, genre INT, duration VARCHAR(8), path VARCHAR(250), album_order INT, plays INT);
INSERT INTO songs() VALUES ();

-- GENRES table
DROP TABLE IF EXISTS `genres`;
CREATE TABLE genres(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(250));

-- ARTISTS table
DROP TABLE IF EXISTS `artists`;
CREATE TABLE artists(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(250));

-- ALBUMS table
DROP TABLE IF EXISTS `albums`;
CREATE TABLE albums(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, title VARCHAR(250), artist INT, genre INT, artwork_path VARCHAR(250));

ALTER TABLE users CHANGE `firstName` `first_name` varchar(250);

UPDATE artists SET name = 'Mickey' WHERE id=2;

INSERT INTO `spotify_clone`.`artists` (`id`, `name`) VALUES (NULL, 'Homer'), (NULL, 'Bart Simpson'), (NULL, 'Bruce Lee');

INSERT INTO `spotify_clone`.`genres` (`id`, `name`) VALUES
(NULL, 'Rock'),
(NULL, 'Pop'),
(NULL, 'Hip Hop'),
(NULL, 'Rap'),
(NULL, 'Classical'),
(NULL, 'Country'),
(NULL, 'Jazz'),
(NULL, 'Folk'),
(NULL, 'Electronic');

INSERT INTO `spotify_clone`.`albums` VALUES
(NULL, 'Bacon and Eggs', 2, 3, 'assets/images/artwork/clearday.jpg'),
(NULL, 'Pizza Head', 3, 6, 'assets/images/artwork/energy.jpg'),
(NULL, 'Sir Fugly', 4, 2, 'assets/images/artwork/funkeyelement.jpg'),
(NULL, 'Summer Hits', 1, 4, 'assets/images/artwork/popdance.jpg'),
(NULL, 'Harold Don\'t Come Over', 2, 1, 'assets/images/artwork/ukulele.jpg');

INSERT INTO `spotify_clone`.`songs` VALUES
(NULL, 'Acoustic Breeze', 1, 5, 8, '2:37', 'assets/music/bensound-acousticbreeze.mp3', 1, 0),
(NULL, 'A new beginning', 1, 5, 1, '2:35', 'assets/music/bensound-anewbeginning.mp3', 2, 0),
(NULL, 'Better Days', 1, 5, 2, '2:33', 'assets/music/bensound-betterdays.mp3', 3, 0),
(NULL, 'Buddy', 1, 5, 3, '2:02', 'assets/music/bensound-buddy.mp3', 4, 0);

INSERT INTO `spotify_clone`.`songs` VALUES
(NULL, 'Clear Day', 1, 5, 4, '1:29', 'assets/music/bensound-clearday.mp3', 5, 0),
(NULL, 'Going Higher', 2, 1, 1, '4:04', 'assets/music/bensound-goinghigher.mp3', 1, 0),
(NULL, 'Funny Song', 2, 4, 2, '3:07', 'assets/music/bensound-funnysong.mp3', 2, 0),
(NULL, 'Funky Element', 2, 1, 3, '3:08', 'assets/music/bensound-funkyelement.mp3', 2, 0),
(NULL, 'Extreme Action', 2, 1, 4, '8:03', 'assets/music/bensound-extremeaction.mp3', 3, 0);

INSERT INTO `spotify_clone`.`songs` VALUES
(NULL, 'Epic', 2, 4, 5, '2:58', 'assets/music/bensound-epic.mp3', 3, 0),
(NULL, 'Energy', 2, 1, 6, '2:59', 'assets/music/bensound-energy.mp3', 4, 0),
(NULL, 'Dubstep', 2, 1, 7, '2:03', 'assets/music/bensound-dubstep.mp3', 5, 0),
(NULL, 'Happiness', 3, 6, 8, '4:21', 'assets/music/bensound-happiness.mp3', 5, 0),
(NULL, 'Happy Rock', 3, 6, 9, '1:45', 'assets/music/bensound-happyrock.mp3', 4, 0),
(NULL, 'Jazzy Frenchy', 3, 6, 10, '1:44', 'assets/music/bensound-jazzyfrenchy.mp3', 3, 0);

INSERT INTO `spotify_clone`.`songs` VALUES
(NULL, 'Little Idea', 3, 6, 1, '2:49', 'assets/music/bensound-littleidea.mp3', 2, 0),
(NULL, 'Memories', 3, 6, 2, '3:50', 'assets/music/bensound-memories.mp3', 1, 0),
(NULL, 'Moose', 4, 7, 1, '2:43', 'assets/music/bensound-moose.mp3', 5, 0),
(NULL, 'November', 4, 7, 2, '3:32', 'assets/music/bensound-november.mp3', 4, 0),
(NULL, 'Of Elias Dream', 4, 7, 3, '4:58', 'assets/music/bensound-ofeliasdream.mp3', 3, 0),
(NULL, 'Pop Dance', 4, 7, 2, '2:42', 'assets/music/bensound-popdance.mp3', 2, 0),
(NULL, 'Retro Soul', 4, 7, 5, '3:36', 'assets/music/bensound-retrosoul.mp3', 1, 0);

INSERT INTO `spotify_clone`.`songs` VALUES
(NULL, 'Sad Day', 5, 2, 1, '2:28', 'assets/music/bensound-sadday.mp3', 1, 0),
(NULL, 'Sci-fi', 5, 2, 2, '4:44', 'assets/music/bensound-scifi.mp3', 2, 0),
(NULL, 'Slow Motion', 5, 2, 3, '3:26', 'assets/music/bensound-slowmotion.mp3', 3, 0),
(NULL, 'Sunny', 5, 2, 4, '2:20', 'assets/music/bensound-sunny.mp3', 4, 0),
(NULL, 'Sweet', 5, 2, 5, '5:07', 'assets/music/bensound-sweet.mp3', 5, 0),
(NULL, 'Tenderness ', 3, 3, 7, '2:03', 'assets/music/bensound-tenderness.mp3', 4, 0),
(NULL, 'The Lounge', 3, 3, 8, '4:16', 'assets/music/bensound-thelounge.mp3 ', 3, 0),
(NULL, 'Ukulele', 3, 3, 9, '2:26', 'assets/music/bensound-ukulele.mp3 ', 2, 0),
(NULL, 'Tomorrow', 3, 3, 1, '4:54', 'assets/music/bensound-tomorrow.mp3 ', 1, 0);
