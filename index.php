<?php
    include('includes/config.php');

    if (isset($_SESSION['userLoggedIn'])) {
        $userLoggedIn = $_SESSION['userLoggedIn'];
    } else {
        header("Location: register.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spotify Clone</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/index.css">
</head>
<body>

<div class="main-container">

    <div class="top-container">
        <?php include('includes/templates/navbar-container.php'); ?>
        <?php include('includes/templates/main-view-container.php'); ?>
    </div>

    <div class="now-playing-bar-container">
        <?php include('includes/templates/now-playing-bar.php'); ?>
    </div> <!-- Now Playing Bar -->
</div>

</body>
</html>