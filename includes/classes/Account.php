<?php
    class Account {

        private $con;
        private $errorArray;

        public function __construct($con) {
            $this->con = $con;
            $this->errorArray = array();
        }

        public function register($un, $fn, $em, $pw, $pw2) {
            $this->validateUsername($un);

            $this->validateFirstName($fn);

            $this->validateEmail($em);

            $this->validatePasswords($pw, $pw2);

            if(empty($this->errorArray)) {
                return $this->insertUserDetails($un, $fn, $em, $pw);
            } else {
                //echo $this->errorArray;
                return false;
            }
        }

        public function getError($error) {
            if(!in_array($error, $this->errorArray)) {
                $error = "";
            }
            return "<span clas='errorMessage'>$error</span>";
        }

        public function login($un, $pw) {
            $pw = md5($pw);

            $query = mysqli_query($this->con, "SELECT * FROM users WHERE username='$un' AND password='$pw'");

            if (mysqli_num_rows($query) == 1) {
                return true;
            } else {
                array_push($this->errorArray, Constants::$loginFailed);
                return false;
            }
        }

        private function insertUserDetails($un, $fn, $em, $pw) {
            $encryptedPw = md5($pw);
            $profilePic = "assets/images/profile-pics/chris-akins.jpg";
            $date = date("Y-m-d");

            $result = mysqli_query($this->con, "INSERT INTO users VALUES (NULL, '$un', '$fn', '$em', '$encryptedPw', '$date', '$profilePic')");
            var_dump($this->con);

            return $result;
        }

        private function validateUsername($un) {
            if (strlen($un) > 32 || strlen($un) < 5) {
                array_push($this->errorArray, Constants::$usernameInvalid);
                return;
            }

            $checkUsernameQuery = mysqli_query($this->con, "SELECT username FROM users WHERE username='$un'");

            if (mysqli_num_rows($checkUsernameQuery) != 0) {
                array_push($this->errorArray, Constants::$usernameTaken);
                return;
            }
        }

        private function validateFirstName($fn) {
            if (strlen($fn) > 32 || strlen($fn) < 2) {
                array_push($this->errorArray, Constants::$firstNameInvalid);
                return;
            }
        }

        private function validateEmail($em) {
            if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
                array_push($this->errorArray, Constants::$emailInvalid);
                return;
            }

            $checkEmailQuery = mysqli_query($this->con, "SELECT email FROM users WHERE email='$em'");

            if (mysqli_num_rows($checkEmailQuery) != 0) {
                array_push($this->errorArray, Constants::$emailTaken);
                return;
            }
        }

        private function validatePasswords($pw, $pw2) {
            if ($pw != $pw2) {
                array_push($this->errorArray, Constants::$passwordsDoNotMatch);
                return;
            }

            if (preg_match('/[^A-Za-z0-9\_\-\@]/', $pw)) {
                array_push($this->errorArray, Constants::$passwordCharacter);
            }
        }
    }
?>