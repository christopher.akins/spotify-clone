<?php
    class Constants {
        public static $passwordsDoNotMatch = "Your passwords do not match.";
        public static $passwordCharacter = "Your password has weird characters in it.";
        public static $emailInvalid = "Your email is not valid.";
        public static $firstNameInvalid = "Your first name needs at least 2 characters.";
        public static $usernameInvalid = "Your username needs to be between 5 and 32 characters.";
        public static $usernameTaken = "This username has been taken. Pick another username";
        public static $emailTaken = "This email has been taken. Pick another email";
        
        public static $loginFailed = "Your username and/or password are incorrect";
    }