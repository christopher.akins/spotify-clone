<?php

    function sanitizePass($inputText) {
        $inputText = strip_tags($inputText);
        return $inputText;
    }
    function sanitizeUsername($inputText) {
        $inputText = strip_tags($inputText);
        $inputText = str_replace(" ", "", $inputText);
        return $inputText;
    }

    function sanitizeString($inputText) {
        $inputText = strip_tags($inputText);
        $inputText = str_replace(" ", "", $inputText);
        $inputText = ucfirst(strtolower($inputText));
        return $inputText;
    }


    if(isset($_POST['registerButton'])) {
        $username = sanitizeUsername($_POST['username']);
        $firstName = sanitizeString($_POST['firstName']);
        $email = sanitizeUsername($_POST['email']);
        $password = sanitizePass($_POST['password']);
        $password2 = sanitizePass($_POST['password2']);

        $validInputs = $account->register($username, $firstName, $email, $password, $password2);

        if ($validInputs == true) {
            $_SESSION['userLoggedIn'] = $username;
            header("Location: index.php");
        }
    }

?>