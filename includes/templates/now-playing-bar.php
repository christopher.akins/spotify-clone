<div class="now-playing-bar">
    <div class="now-playing-left">
        <div class="content">
            <div class="album-link">
                <img src="https://via.placeholder.com/150" alt="" class="album-artwork">
            </div>
            <div class="track-info">
                <span class="track-name">Thuggish Ruggish Bone</span>
                <span class="artist-name">

                </span>
            </div>
        </div>
    </div>
    <div class="now-playing-center">
        <div class="content player-controls">
            <div class="buttons">
                <button class="control-button shuffle">
                    <img src="assets/images/icons/shuffle.png" alt="">
                </button>
                <button class="control-button previous">
                    <img src="assets/images/icons/previous.png" alt="">
                </button>
                <button class="control-button play">
                    <img src="assets/images/icons/play.png" alt="">
                </button>
                <button class="control-button pause" style="display: none">
                    <img src="assets/images/icons/pause.png" alt="">
                </button>
                <button class="control-button next">
                    <img src="assets/images/icons/next.png" alt="">
                </button>
                <button class="control-button repeat">
                    <img src="assets/images/icons/repeat.png" alt="">
                </button>
            </div>

            <div class="playback-bar">
                <div class="progress-time current">0.00</div>
                <div class="progress-bar">
                    <div class="progress-bar-bg">
                        <div class="progress"></div>
                    </div>
                </div>
                <div class="progress-time remaining">0.00</div>
            </div>
        </div>
    </div>
    <div class="now-playing-right">
        <div class="volume-bar">
            <button class="control-button volume" title="Volume button">
                <img src="assets/images/icons/volume.png" alt="Volume">
            </button>
            <div class="progress-bar">
                <div class="progress-bar-bg">
                    <div class="progress"></div>
                </div>
            </div>
        </div>
    </div>

</div>