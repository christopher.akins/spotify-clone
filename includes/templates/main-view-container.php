<div class="main-view-container">
    <div class="main-content">
        <h2 class="page-heading-big">You might also Like</h2>
        <div class="grid-view-container">
            <?php
                $albumQuery = mysqli_query($con, "SELECT * FROM albums");

                while($row = mysqli_fetch_array($albumQuery)) {
                    echo $row['title'] . "<br>";
                }
            ?>
        </div>
    </div>
</div>