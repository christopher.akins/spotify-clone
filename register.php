<?php

    include('includes/config.php');
    include('includes/classes/Constants.php');
    include('includes/classes/Account.php');

    $account = new Account($con);
    include('includes/handlers/register-handler.php');
    include('includes/handlers/login-handler.php');

    function getInputValue($name) {
        if(isset($_POST[$name])) {
            echo $_POST[$name];
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/register.css">
    <?php if (isset($_POST['registerButton'])) : ?>
        <style>
            .login-wrapper { display: none; }
            .register-wrapper { display: block; }
        </style>
    <?php else : ?>
        <style>
            .login-wrapper { display: block; }
            .register-wrapper { display: none; }
        </style>
    <?php endif; ?>
</head>
<body>

    <div class="register-container">
        <div id="inputContainer">
            <div class="login-wrapper">
                <h2>Login to your account:</h2>
                <form id="loginForm" action="register.php" method="POST">
                    <?php echo $account->getError(Constants::$loginFailed); ?>
                    <label for="loginUsername">Username:</label>
                    <input type="text" name="loginUsername" id="loginUsername" placeholder="e.g. John Smith" value="<?php getInputValue('loginUsername') ?>"required>
                    <label for="loginPassword">Password:</label>
                    <input type="password" name="loginPassword" id="loginPassword" required>
                    <input type="hidden" value="testHiddenLogin" name="testHiddenLogin">

                    <button type="submit" name="loginButton">Log In</button>

                    <div class="has-account-text">
                        <span id="hideLogin">Don't have an account yet? Signup here.</span>
                    </div>
                </form>
            </div>
            <div class="register-wrapper">
                <h2>Create your free account:</h2>
                <form id="registerForm" action="register.php" method="POST">
                    <?php echo $account->getError(Constants::$usernameInvalid); ?>
                    <?php echo $account->getError(Constants::$usernameTaken); ?>
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="username" placeholder="e.g. John Smith" value="<?php getInputValue('usernamme'); ?>" required>

                    <?php echo $account->getError(Constants::$firstNameInvalid); ?>
                    <label for="firstName">First Name:</label>
                    <input type="text" name="firstName" id="firstName" placeholder="e.g. John Smith" value="<?php getInputValue('firstName'); ?>"required>

                    <?php echo $account->getError(Constants::$emailInvalid); ?>
                    <?php echo $account->getError(Constants::$emailTaken); ?>
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" placeholder="e.g. youremail@gmail.com" value="<?php getInputValue('email'); ?>" required>

                    <?php echo $account->getError(Constants::$passwordCharacter); ?>
                    <?php echo $account->getError(Constants::$passwordsDoNotMatch); ?>
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" required>
                    <label for="password2">Confirm Password:</label>
                    <input type="password" name="password2" id="password2" required>

                    <button type="submit" name="registerButton">Sign Up</button>

                    <div class="has-account-text">
                        <span id="hideRegister">Already have an account? Login here.</span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="assets/js/register.js"></script>
</body>
</html>